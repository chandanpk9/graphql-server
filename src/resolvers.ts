import db from "../db/_db.js";

export const resolvers = {
  Query: {
    authors() {
      return db.authors;
    },
    games() {
      return db.games;
    },
    reviews() {
      return db.reviews;
    },
  },
};
