export const typeDefs = `#graphql
  type Game {
    id: ID!
    title: String!
    paltform: [String!]!
  }

  type Author {
    id: ID!
    name: String!
    verified: Boolean!
  }

  type Review {
    id: ID!
    rating: Int!
    content: String!
  }

  # Entry point for all queries
  type Query {
    authors: [Author]
    games: [Game]
    reviews: [Review]
  }
`;
